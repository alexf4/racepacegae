import cgi

import os
try:
  from lxml import etree
  print("running with lxml.etree")
except ImportError:
  try:
    # Python 2.5
    import xml.etree.cElementTree as etree
    #print("running with cElementTree on Python 2.5+")
  except ImportError:
    try:
      # Python 2.5
      import xml.etree.ElementTree as etree
      #print("running with ElementTree on Python 2.5+")
    except ImportError:
      try:
        # normal cElementTree install
        import cElementTree as etree
        #print("running with cElementTree")
      except ImportError:
        try:
          # normal ElementTree install
          import elementtree.ElementTree as etree
          #print("running with ElementTree")
        except ImportError:
          print("Failed to import ElementTree from any known place")
from google.appengine.ext.webapp import template

from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db

from taxi import taxi

class MainPage(webapp.RequestHandler):
    def get(self):
        #self.response.out.write('<html><body>')
        
        root = etree.Element("root")
        
        
        #resturants = doc.createElementNS("Ames", "Location")
        
        #doc.appendChild(resturants)

        #resturant_query = resturant.all()
        
        #print resturant_query
        cabs = db.GqlQuery("SELECT * FROM taxi")
        
        for car in cabs:
            element = etree.Element("taxi", name = car.name , phoneNumber = car.phoneNumber)
            root.append(element)
            
            #self.response.out.writelines('<b> ' +foodplace.name +  '</b>')
        self.response.out.write(self.getXML(root))
        
        # self.response.out.write('</body></html>')
    
    def getXML(self , root):
      #Figure out the number of branches
      number_of_branches = len(root.getchildren() ) 
      
      #Start the XML tag
      return_string = "<?xml version='1.0'?> \n"

      #Create an iterator to run down the tree
      squirl = root.getiterator()
      
      squirl.next()
      
      return_string += "<root>"
      
      
      while number_of_branches:
        #Looks like we found what we are looking for
        nut = squirl.next()
        
        #Create the XML element
        xml_element = '<taxi><name>%s</name><phoneNumber>%s</phoneNumber></taxi>' \
        %(nut.get("name"), nut.get("phoneNumber")) 
        return_string += xml_element
        
        number_of_branches = number_of_branches -1 
        
      return_string += "</root>"
      
        
      return return_string


application = webapp.WSGIApplication([('/viewAmesTaxi', MainPage)],debug=True)
        
def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()