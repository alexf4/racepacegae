import cgi

import os
import urllib

from maptest import findLatLon


try:
  from lxml import etree
  print("running with lxml.etree")
except ImportError:
  try:
    # Python 2.5
    import xml.etree.cElementTree as etree
    #print("running with cElementTree on Python 2.5+")
  except ImportError:
    try:
      # Python 2.5
      import xml.etree.ElementTree as etree
      #print("running with ElementTree on Python 2.5+")
    except ImportError:
      try:
        # normal cElementTree install
        import cElementTree as etree
        #print("running with cElementTree")
      except ImportError:
        try:
          # normal ElementTree install
          import elementtree.ElementTree as etree
          #print("running with ElementTree")
        except ImportError:
          print("Failed to import ElementTree from any known place")
from google.appengine.ext.webapp import template

from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db
    
class resturant(db.Model):
    name = db.StringProperty(required=True)
    location = db.StringProperty()
    day = db.StringProperty()
    special = db.StringProperty()
    
    lat = db.StringProperty()
    lon = db.StringProperty()
    
class user(db.Model):
    name = db.StringProperty()
    account = db.UserProperty()
    id_number = db.IntegerProperty()
    active = db.BooleanProperty()
    phone = db.PhoneNumberProperty()
    

        
class MainPage(webapp.RequestHandler):
    def get(self):
        self.response.out.write('<html><body>')
        
        #self.response.out.write(""" Select Day: <form NAME= "myform" action="">
        #<select name="Day">
        #<option value="Monday">Monday</option>
        #<option value="Tuesday">Tuesday</option>
        #<option value="Wed" selected="selected">Wed</option>
        #<option value="Thursday">Thursday</option>
        #<option value="Friday">Friday</option>
        #<option value="Saturday">Saturday</option>
        #<option value="Sunday">Sunday</option>
        #</select>
        #</form>
        #""")
        

        # Write the submission form and the footer of the page
        self.response.out.write("""
              <form action="/sign" method="post">
                <div>Restaurant name: <input type="text" name="resturantname" /><br /></div>
                
                <div>Location: <input type="text" name="location" /></div>
                <div>Special: <input type="text" name="Special" /></div>
                <div>Day: <input type="text" name="Day" /></div>
                
                <div><input type="submit" value="Enter Data"></div>
                
                
                
                
              </form>""")
        
        self.response.out.write("""
              <form action="/view" method="post">
                <div><input type="submit" value="View Data"></div>
              </form>
              
            </body>
          </html>""")

class Guestbook(webapp.RequestHandler):
  def post(self):
    
    food = resturant(name =self.request.get('resturantname') )
    food.name = self.request.get('resturantname')
    food.location = self.request.get('location')
    food.day = self.request.get('Day')
    food.special = self.request.get('Special')
    
    #temp = self.findLatLong(self.request.get('location'))
    
    temp = findLatLon(self.request.get('location'))
    
    food.lat = temp[0]
    food.lon = temp[1]
    
    food.put()
    self.redirect('/')

  
        
        
        #greeting = Greeting()

        #if users.get_current_user():
            #greeting.author = users.get_current_user()

        #greeting.content = self.request.get('content')
        #greeting.put()
        
        
class XMLData(webapp.RequestHandler):
    def post(self):
        #self.response.out.write('<html><body>')
        
        root = etree.Element("root")
        
        
        #resturants = doc.createElementNS("Ames", "Location")
        
        #doc.appendChild(resturants)

        resturant_query = resturant.all()
        
        #print resturant_query
        restaurents = db.GqlQuery("SELECT * FROM resturant")
        
        for foodplace in restaurents:
            element = etree.Element("resturant", name = foodplace.name , location = foodplace.location, day = foodplace.day, special = foodplace.special)
            root.append(element)
            
            #self.response.out.writelines('<b> ' +foodplace.name +  '</b>')
        self.response.out.write(self.getXML(root))
        
        # self.response.out.write('</body></html>')
    
    def getXML(self , root):
      #Figure out the number of branches
      number_of_branches = len(root.getchildren() ) 
      
      #Start the XML tag
      return_string = "<?xml version='1.0'?> \n"

      #Create an iterator to run down the tree
      squirl = root.getiterator()
      
      squirl.next()
      
      return_string += "<root>"
      
      
      while number_of_branches:
        #Looks like we found what we are looking for
        nut = squirl.next()
        
        #Create the XML element
        xml_element = '<resturant><name>%s</name><location>%s</location><day>%s</day><special>%s</special></resturant>' \
        %(nut.get("name"), nut.get("location"), nut.get("day"), nut.get("special")) 
        return_string += xml_element
        
        number_of_branches = number_of_branches -1 
        
      return_string += "</root>"
      
        
      return return_string

application = webapp.WSGIApplication(
                                     [('/', MainPage),
                                      ('/sign', Guestbook),
                                      ('/view', XMLData)
                                      ],
                                     debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()